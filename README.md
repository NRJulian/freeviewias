# FreeViewIAS
FreeViewIAS is a free and open-source desktop app for viewing and exporting IAS RADIUS logs. 

![Screenshot](./Screenshots/screenshot_1.png)

FreeViewIAS is a WPF desktop application written in .NET 5 with Visual Studio 2019. It can open IAS RADIUS logs for perusal and export the logs into the more manageable CSV format. It is a minimal application that serves its sole purpose with reliable simplicity.

## Installation

FreeViewIAS is a standalone Windows executable: simply download the latest release from the releases page [here](https://gitlab.com/NRJulian/freeviewias/-/releases) and double-click the .exe to run (after checking the hash and running it through anti-virus, of course).  

## Licensing

FreeViewIAS is free and open-source software licensed under the [MIT license](https://choosealicense.com/licenses/mit/).