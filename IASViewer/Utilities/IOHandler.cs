﻿using FreeViewIAS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace FreeViewIAS.Utilities
{
    public static class IOHandler
    {
        public static async void WriteIASEntriesToCSVAsync(List<IASLogEntry> entries, string filePath)
        {
            List<String> csvStrings = new List<String>();
            foreach (IASLogEntry entry in entries) 
            {
                csvStrings.Add(entry.ToString());
            }
            try 
            {
                await File.WriteAllTextAsync(filePath, IASLogEntry.headers + Environment.NewLine);
                await File.AppendAllLinesAsync(filePath, csvStrings);
                MessageBox.Show("File saved successfully to " + filePath, "Export Success", MessageBoxButton.OK, MessageBoxImage.Information);
            } 
            catch 
            {
                MessageBox.Show("Failed to export data to CSV file.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static IASImportPackage LoadIASFile(String filePath)
        {
            IASImportPackage importResult = new IASImportPackage();
            string[] fileLines = File.ReadAllLines(filePath);
            foreach (String line in fileLines) 
            {
                try
                {
                    importResult.AddIASLogEntry(new IASLogEntry(line.Split(",")));
                }
                catch 
                {
                    importResult.AddFailedLine(line);
                }
            }
            if (importResult.GetIASEntries().Count > 0) {
                return importResult;
            } 
            else 
            {
                throw new Exception("Could not read log file. Is the format correct?");
            }
        }
    }
}
