﻿using System;
using System.Collections.Generic;

namespace FreeViewIAS.Models
{
    public class IASImportPackage
    {
        private List<IASLogEntry> entries = new List<IASLogEntry>();
        private List<String> failedLines = new List<string>();

        public List<IASLogEntry> GetIASEntries() 
        {
            return entries;
        }

        public List<String> GetFailedLines() 
        {
            return failedLines;
        }        

        public void AddIASLogEntry(IASLogEntry entry) 
        {
            this.entries.Add(entry);
        }

        public void AddFailedLine(String line) 
        {
            this.failedLines.Add(line);
        }
    }
}
