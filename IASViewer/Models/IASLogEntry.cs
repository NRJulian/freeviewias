﻿using System;
using System.Text;

namespace FreeViewIAS.Models
{
    public class IASLogEntry
    {
        public String computerName { get; set; }
        public String serviceName { get; set; }
        public String recordDate { get; set; }
        public String recordTime { get; set; }
        public String packetType { get; set; }
        public String userName { get; set; }
        public String fullyQualifiedDistinguishedName { get; set; }
        public String calledStationID { get; set; }
        public String callingStationID { get; set; }
        public String callbackNumber { get; set; }
        public String framedIPAddress { get; set; }
        public String nasIdentifier { get; set; }
        public String nasIPAddress { get; set; }
        public String nasPort { get; set; }
        public String clientVendor { get; set; }
        public String clientIPAddress { get; set; }
        public String clientFriendlyName { get; set; }
        public String eventTimestamp { get; set; }
        public String portLimit { get; set; }
        public String nasPortType { get; set; }
        public String connectInfo { get; set; }
        public String framedProtocol { get; set; }
        public String serviceType { get; set; }
        public String authenticationType { get; set; }
        public String policyName { get; set; }
        public String reasonCode { get; set; }
        public String classAttribute { get; set; }
        public String sessionTimeout { get; set; }
        public String idleTimeout { get; set; }
        public String terminationAction { get; set; }
        public String eapFriendlyName { get; set; }
        public String acctStatusType { get; set; }
        public String acctDelayTime { get; set; }
        public String acctInputOctets { get; set; }
        public String acctOutputOctets { get; set; }
        public String acctSessionId { get; set; }
        public String acctAuthentic { get; set; }
        public String acctSessionTime { get; set; }
        public String acctInputPackets { get; set; }
        public String acctOutputPackets { get; set; }
        public String acctTerminateCause { get; set; }
        public String acctMultiSsnID { get; set; }
        public String acctLinkCount { get; set; }
        public String acctInterimInterval { get; set; }
        public String tunnelType { get; set; }
        public String tunnelMediumType { get; set; }
        public String tunnelClientEndpt { get; set; }
        public String tunnelServerEndpt { get; set; }
        public String acctTunnelConn { get; set; }
        public String tunnelPvtGroupID { get; set; }
        public String tunnelAssignmentID { get; set; }
        public String tunnelPreference { get; set; }
        public String msAcctAuthType { get; set; }
        public String msAcctEAPType { get; set; }
        public String msRASVersion { get; set; }
        public String msRASVendor { get; set; }
        public String msCHAPError { get; set; }
        public String msCHAPDomain { get; set; }
        public String msMPPEEncryptionTypes { get; set; }
        public String msMPPEEncryptionPolicy { get; set; }
        public String proxyPolicyName { get; set; }
        public String providerType { get; set; }
        public String providerName { get; set; }
        public String remoteServerAddress { get; set; }
        public String mSRASClientName { get; set; }
        public String mSRASClientVersion { get; set; }
        public static String headers { get; set; }

        public IASLogEntry(String[] values)
        {
            this.computerName = values[0].Replace("\"","");
            this.serviceName = values[1].Replace("\"","");
            this.recordDate = values[2].Replace("\"","");
            this.recordTime = values[3].Replace("\"","");
            this.packetType = TranslatePacketType(values[4]);
            this.userName = values[5].Replace("\"","");
            this.fullyQualifiedDistinguishedName = values[6].Replace("\"","");
            this.calledStationID = values[7].Replace("\"","");
            this.callingStationID = values[8].Replace("\"","");
            this.callbackNumber = values[9].Replace("\"","");
            this.framedIPAddress = values[10].Replace("\"","");
            this.nasIdentifier = values[11].Replace("\"","");
            this.nasIPAddress = values[12].Replace("\"","");
            this.nasPort = values[13].Replace("\"","");
            this.clientVendor = values[14].Replace("\"","");
            this.clientIPAddress = values[15].Replace("\"","");
            this.clientFriendlyName = values[16].Replace("\"","");
            this.eventTimestamp = values[17].Replace("\"","");
            this.portLimit = values[18].Replace("\"","");
            this.nasPortType = values[19].Replace("\"","");
            this.connectInfo = values[20].Replace("\"","");
            this.framedProtocol = values[21].Replace("\"","");
            this.serviceType = TranslateServiceType(values[22]);
            this.authenticationType = TranslateAuthenticationType(values[23]);
            this.policyName = values[24].Replace("\"","");
            this.reasonCode = TranslateReasonCode(values[25]);
            this.classAttribute = values[26].Replace("\"","");
            this.sessionTimeout = values[27].Replace("\"","");
            this.idleTimeout = values[28].Replace("\"","");
            this.terminationAction = TranslateTerminateCause(values[29]);
            this.eapFriendlyName = values[30].Replace("\"","");
            this.acctStatusType = TranslateStatusType(values[31]);
            this.acctDelayTime = values[32].Replace("\"","");
            this.acctInputOctets = values[33].Replace("\"","");
            this.acctOutputOctets = values[34].Replace("\"","");
            this.acctSessionId = values[35].Replace("\"","");
            this.acctAuthentic = TranslateAccountAuthenticationType(values[36]);
            try
            {
                var sessionTimeInSeconds = TimeSpan.FromSeconds(long.Parse(values[37]));
                this.acctSessionTime = string.Format("{0:D2}h:{1:D2}m:{2:D2}s:{3:D3}ms",
                    sessionTimeInSeconds.Hours,
                    sessionTimeInSeconds.Minutes,
                    sessionTimeInSeconds.Seconds,
                    sessionTimeInSeconds.Milliseconds);
            }
            catch
            {
                this.acctSessionTime = values[37].Replace("\"","");
            }
            this.acctInputPackets = values[38].Replace("\"","");
            this.acctOutputPackets = values[39].Replace("\"","");
            this.acctTerminateCause = values[40].Replace("\"","");
            this.acctMultiSsnID = values[41].Replace("\"","");
            this.acctLinkCount = values[42].Replace("\"","");
            this.acctInterimInterval = values[43].Replace("\"","");
            this.tunnelType = values[44].Replace("\"","");
            this.tunnelMediumType = values[45].Replace("\"","");
            this.tunnelClientEndpt = values[46].Replace("\"","");
            this.tunnelServerEndpt = values[47].Replace("\"","");
            this.acctTunnelConn = values[48].Replace("\"","");
            this.tunnelPvtGroupID = values[49].Replace("\"","");
            this.tunnelAssignmentID = values[50].Replace("\"","");
            this.tunnelPreference = values[51].Replace("\"","");
            this.msAcctAuthType = values[52].Replace("\"","");
            this.msAcctEAPType = values[53].Replace("\"","");
            this.msRASVersion = values[54].Replace("\"","");
            this.msRASVendor = values[55].Replace("\"","");
            this.msCHAPError = values[56].Replace("\"","");
            this.msCHAPDomain = values[57].Replace("\"","");
            this.msMPPEEncryptionTypes = values[58].Replace("\"","");
            this.msMPPEEncryptionPolicy = values[59].Replace("\"","");
            this.proxyPolicyName = values[60].Replace("\"","");
            this.providerType = values[61].Replace("\"","");
            this.providerName = values[62].Replace("\"","");
            this.remoteServerAddress = values[63].Replace("\"","");
            this.mSRASClientName = values[64].Replace("\"","");
            this.mSRASClientVersion = values[65].Replace("\"","");

            //This code and the ToString override is unorthodox but prevents the need to
            //import third-party libraries and deal with third-party licensing for a very limited use.
            StringBuilder stringBuilder = new StringBuilder()
                .Append("ComputerName,")
                .Append("ServiceName,")
                .Append("RecordDate,")
                .Append("RecordTime,")
                .Append("PacketType,")
                .Append("UserName,")
                .Append("FullyQualifiedDistinguishedName,")
                .Append("CalledStationID,")
                .Append("CallingStationID,")
                .Append("CallbackNumber,")
                .Append("FramedIPAddress,")
                .Append("NasIdentifier,")
                .Append("NasIPAddress,")
                .Append("NasPort,")
                .Append("ClientVendor,")
                .Append("ClientIPAddress,")
                .Append("ClientFriendlyName,")
                .Append("EventTimestamp,")
                .Append("PortLimit,")
                .Append("NasPortType,")
                .Append("ConnectInfo,")
                .Append("FramedProtocol,")
                .Append("ServiceType,")
                .Append("AuthenticationType,")
                .Append("PolicyName,")
                .Append("ReasonCode,")
                .Append("ClassAttribute,")
                .Append("SessionTimeout,")
                .Append("IdleTimeout,")
                .Append("TerminationAction,")
                .Append("EapFriendlyName,")
                .Append("AcctStatusType,")
                .Append("AcctDelayTime,")
                .Append("AcctInputOctets,")
                .Append("AcctOutputOctets,")
                .Append("AcctSessionId,")
                .Append("AcctAuthentic,")
                .Append("AcctSessionTime,")
                .Append("AcctInputPackets,")
                .Append("AcctOutputPackets,")
                .Append("AcctTerminateCause,")
                .Append("AcctMultiSsnID,")
                .Append("AcctLinkCount,")
                .Append("AcctInterimInterval,")
                .Append("TunnelType,")
                .Append("TunnelMediumType,")
                .Append("TunnelClientEndpt,")
                .Append("TunnelServerEndpt,")
                .Append("AcctTunnelConn,")
                .Append("TunnelPvtGroupID,")
                .Append("TunnelAssignmentID,")
                .Append("TunnelPreference,")
                .Append("MsAcctAuthType,")
                .Append("MsAcctEAPType,")
                .Append("MsRASVersion,")
                .Append("MsRASVendor,")
                .Append("MsCHAPError,")
                .Append("MsCHAPDomain,")
                .Append("MsMPPEEncryptionTypes,")
                .Append("MsMPPEEncryptionPolicy,")
                .Append("ProxyPolicyName,")
                .Append("ProviderType,")
                .Append("ProviderName,")
                .Append("RemoteServerAddress,")
                .Append("MSRASClientName,")
                .Append("MSRASClientVersion");

            IASLogEntry.headers = stringBuilder.ToString();
        }

        public override String ToString()
        {
            StringBuilder stringBuilder = new StringBuilder()
                .Append($"{this.computerName},")
                .Append($"{this.serviceName},")
                .Append($"{this.recordDate},")
                .Append($"{this.recordTime},")
                .Append($"{this.packetType},")
                .Append($"{this.userName},")
                .Append($"{this.fullyQualifiedDistinguishedName},")
                .Append($"{this.calledStationID},")
                .Append($"{this.callingStationID},")
                .Append($"{this.callbackNumber},")
                .Append($"{this.framedIPAddress},")
                .Append($"{this.nasIdentifier},")
                .Append($"{this.nasIPAddress},")
                .Append($"{this.nasPort},")
                .Append($"{this.clientVendor},")
                .Append($"{this.clientIPAddress},")
                .Append($"{this.clientFriendlyName},")
                .Append($"{this.eventTimestamp},")
                .Append($"{this.portLimit},")
                .Append($"{this.nasPortType},")
                .Append($"{this.connectInfo},")
                .Append($"{this.framedProtocol},")
                .Append($"{this.serviceType},")
                .Append($"{this.authenticationType},")
                .Append($"{this.policyName},")
                .Append($"{this.reasonCode},")
                .Append($"{this.classAttribute},")
                .Append($"{this.sessionTimeout},")
                .Append($"{this.idleTimeout},")
                .Append($"{this.terminationAction},")
                .Append($"{this.eapFriendlyName},")
                .Append($"{this.acctStatusType},")
                .Append($"{this.acctDelayTime},")
                .Append($"{this.acctInputOctets},")
                .Append($"{this.acctOutputOctets},")
                .Append($"{this.acctSessionId},")
                .Append($"{this.acctAuthentic},")
                .Append($"{this.acctSessionTime},")
                .Append($"{this.acctInputPackets},")
                .Append($"{this.acctOutputPackets},")
                .Append($"{this.acctTerminateCause},")
                .Append($"{this.acctMultiSsnID},")
                .Append($"{this.acctLinkCount},")
                .Append($"{this.acctInterimInterval},")
                .Append($"{this.tunnelType},")
                .Append($"{this.tunnelMediumType},")
                .Append($"{this.tunnelClientEndpt},")
                .Append($"{this.tunnelServerEndpt},")
                .Append($"{this.acctTunnelConn},")
                .Append($"{this.tunnelPvtGroupID},")
                .Append($"{this.tunnelAssignmentID},")
                .Append($"{this.tunnelPreference},")
                .Append($"{this.msAcctAuthType},")
                .Append($"{this.msAcctEAPType},")
                .Append($"{this.msRASVersion},")
                .Append($"{this.msRASVendor},")
                .Append($"{this.msCHAPError},")
                .Append($"{this.msCHAPDomain},")
                .Append($"{this.msMPPEEncryptionTypes},")
                .Append($"{this.msMPPEEncryptionPolicy},")
                .Append($"{this.proxyPolicyName},")
                .Append($"{this.providerType},")
                .Append($"{this.providerName},")
                .Append($"{this.remoteServerAddress},")
                .Append($"{this.mSRASClientName},")
                .Append($"{this.mSRASClientVersion}");

            return stringBuilder.ToString();
        }

        //Translation codes for these values were cross-referenced and correlated from
        //https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/dd349801(v=ws.10)
        //and https://social.technet.microsoft.com/Forums/en-US/32b5eaec-7a2f-4965-8bc8-b6adbbbb9f13/iasnps-log-parsing
        private String TranslatePacketType(String value)
        {
            switch (value)
            {
                case "": return "";
                case "1": return "Access-Request";
                case "2": return "Access-Accept";
                case "3": return "Access-Reject";
                case "4": return "Accounting-Request";
                case "5": return "Accounting-Response";
                case "6": return "Accounting-Status";
                case "7": return "Password-Request";
                case "8": return "Password-Ack";
                case "9": return "Password-Reject";
                case "10": return "Accounting-Message";
                case "11": return "Access-Challenge";
                case "21": return "Resource-Free-Request";
                case "22": return "Resource-Free-Response";
                case "23": return "Resource-Query-Request";
                case "24": return "Resource-Query-Response";
                case "25": return "Alternate-Resource-Reclaim-Request";
                case "26": return "NAS-Reboot-Request";
                case "27": return "NAS-Reboot-Response";
                case "29": return "Next-Passcode";
                case "30": return "New-Pin";
                case "31": return "Terminate-Session";
                case "32": return "Password-Expired";
                case "33": return "Event-Request";
                case "34": return "Event-Response";
                case "40": return "Disconnect-Request";
                case "41": return "Disconnect-ACK";
                case "42": return "Disconnect-NAK";
                case "43": return "CoA-Request";
                case "44": return "CoA-ACK";
                case "45": return "CoA-NAK";
                case "50": return "IP-Address-Allocate";
                case "51": return "IP-Address-Release";
                default: return "Unrecognized value: " + value;
            }
        }

        private String TranslateServiceType(String value)
        {
            switch (value)
            {
                case "": return "";
                case "1": return "Login";
                case "2": return "Framed";
                case "3": return "Callback Login";
                case "4": return "Callback Framed";
                case "5": return "Outbound";
                case "6": return "Administrative";
                case "7": return "NAS Prompt";
                case "8": return "Authenticate Only";
                case "9": return "Callback NAS Prompt";
                case "10": return "Call Check";
                case "11": return "Callback Administrative";
                case "12": return "Voice";
                case "13": return "Fax";
                case "14": return "Modem Relay";
                case "15": return "IAPP -Register";
                case "16": return "IAPP -AP-Check";
                case "17": return "Authorize Only";
                case "18":
                    return "Framed -Management";
                case "19":
                    return "Additional -Authorization";
                default: return "Unrecognized value: " + value;
            }
        }

        private String TranslateAuthenticationType(String value)
        {
            switch (value)
            {
                case "": return "";
                case "1": return "PAP";
                case "2": return "CHAP";
                case "3": return "MS-CHAP";
                case "4": return "MS-CHAP v2";
                case "5": return "EAP";
                case "7": return "None";
                case "8": return "Custom";
                case "11": return "PEAP";
                default: return "Unrecognized value: " + value;
            }
        }       

        private String TranslateReasonCode(String value)
        {
            switch (value)
            {
                case "": return "";
                case "0": return "IAS_SUCCESS";
                case "1": return "IAS_INTERNAL_ERROR";
                case "2": return "IAS_ACCESS_DENIED";
                case "3": return "IAS_MALFORMED_REQUEST";
                case "4": return "IAS_GLOBAL_CATALOG_UNAVAILABLE";
                case "5": return "IAS_DOMAIN_UNAVAILABLE";
                case "6": return "IAS_SERVER_UNAVAILABLE";
                case "7": return "IAS_NO_SUCH_DOMAIN";
                case "8": return "IAS_NO_SUCH_USER";
                case "16": return "IAS_AUTH_FAILURE";
                case "17": return "IAS_CHANGE_PASSWORD_FAILURE";
                case "18": return "IAS_UNSUPPORTED_AUTH_TYPE";
                case "32": return "IAS_LOCAL_USERS_ONLY";
                case "33": return "IAS_PASSWORD_MUST_CHANGE";
                case "34": return "IAS_ACCOUNT_DISABLED";
                case "35": return "IAS_ACCOUNT_EXPIRED";
                case "36": return "IAS_ACCOUNT_LOCKED_OUT";
                case "37": return "IAS_INVALID_LOGON_HOURS";
                case "38": return "IAS_ACCOUNT_RESTRICTION";
                case "48": return "IAS_NO_POLICY_MATCH";
                case "64": return "IAS_DIALIN_LOCKED_OUT";
                case "65": return "IAS_DIALIN_DISABLED";
                case "66": return "IAS_INVALID_AUTH_TYPE";
                case "67": return "IAS_INVALID_CALLING_STATION";
                case "68": return "IAS_INVALID_DIALIN_HOURS";
                case "69": return "IAS_INVALID_CALLED_STATION";
                case "70": return "IAS_INVALID_PORT_TYPE";
                case "71": return "IAS_INVALID_RESTRICTION";
                case "80": return "IAS_NO_RECORD";
                case "96": return "IAS_SESSION_TIMEOUT";
                case "97": return "IAS_UNEXPECTED_REQUEST";
                default: return "Unrecognized value: " + value;
            }
        }

        private String TranslateTerminateCause(String value)
        {
            switch (value)
            {
                case "": return "";
                case "1": return "User Request";
                case "2": return "Lost Carrier";
                case "3": return "Lost Service";
                case "4": return "Idle Timeout";
                case "5": return "Session Timeout";
                case "6": return "Admin Reset";
                case "7": return "Admin Reboot";
                case "8": return "Port Error";
                case "9": return "NAS Error";
                case "10": return "NAS Request";
                case "11": return "NAS Reboot";
                case "12": return "Port Unneeded";
                case "13": return "Port Preempted";
                case "14": return "Port Suspended";
                case "15": return "Service Unavailable";
                case "16": return "Callback";
                case "17": return "User Error";
                case "18": return "Host Request";
                case "19": return "Supplicant Restart";
                case "20": return "Reauthentication Failure";
                case "21": return "Port Reinitialized";
                case "22": return "Port Administratively Disabled";
                case "23": return "Lost Power";
                default: return "Unrecognized value: " + value;
            }

        }

        private String TranslateStatusType(String value)
        {
            switch (value)
            {
                case "": return "";
                case "1": return "Start";
                case "2": return "Stop";
                case "3": return "Interim-Update";
                case "7": return "Accounting-On";
                case "8": return "Accounting-Off";
                case "9": return "Tunnel-Start";
                case "10": return "Tunnel-Stop";
                case "11": return "Tunnel-Reject";
                case "12": return "Tunnel-Link-Start";
                case "13": return "Tunnel-Link-Stop";
                case "14": return "Tunnel-Link-Reject";
                case "15": return "Failed";
                default: return "Unrecognized value: " + value;
            }
        }

        private String TranslateAccountAuthenticationType(String value)
        {
            switch (value)
            {
                case "": return "";
                case "1": return "RADIUS";
                case "2": return "Local";
                case "3": return "Remote";
                case "4": return "Diameter";
                default: return "Unrecognized value: " + value;
            }
        }
    }
}
