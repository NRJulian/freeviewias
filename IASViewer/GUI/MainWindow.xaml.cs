﻿using FreeViewIAS.Models;
using FreeViewIAS.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace FreeViewIAS.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<IASLogEntry> entries = new List<IASLogEntry>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void loadDetailWindow(object sender, MouseButtonEventArgs e)
        {
            if (iasLogEntryListView.SelectedIndex >= 0)
            {
                DetailWindow detail = new DetailWindow(entries.ElementAt(iasLogEntryListView.SelectedIndex));
                detail.Show();
            }
        }

        private void exitMenuItemClicked(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void openMenuItemClick(object sender, RoutedEventArgs e)
        {
            String filePath = DialogHandler.GetFileToOpen();
            if (String.IsNullOrEmpty(filePath)) { return; }
            try
            {
                IASImportPackage importResult = IOHandler.LoadIASFile(filePath);
                this.entries = importResult.GetIASEntries();
                iasLogEntryListView.ItemsSource = entries;
                if (importResult.GetFailedLines().Count > 0)
                {
                    MessageBox.Show($"Imported {importResult.GetIASEntries().Count} lines, but {importResult.GetFailedLines().Count} lines could not be read.\nYou may want to check the file format.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch
            {
                MessageBox.Show("Unable to parse file.\nIs the IAS format correct?", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void exportMenuItemClicked(object sender, RoutedEventArgs e)
        {
            if (entries.Count == 0) 
            {
                MessageBox.Show("No IAS file loaded to export.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            String filePath = DialogHandler.GetFileToSave();
            if (String.IsNullOrEmpty(filePath)){ return; }
            try
            {
                IOHandler.WriteIASEntriesToCSVAsync(entries, filePath);
            }
            catch
            {
                MessageBox.Show("Failed to export data to CSV file.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void aboutMenuItemClicked(object sender, RoutedEventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.Show();
        }
    }
}
