﻿using FreeViewIAS.Models;
using System.Windows;

namespace FreeViewIAS.GUI
{
    /// <summary>
    /// Interaction logic for DetailWindow.xaml
    /// </summary>
    public partial class DetailWindow : Window
    {
        public DetailWindow(IASLogEntry entry)
        {
            InitializeComponent();
            loadData(entry);
        }

        private void loadData(IASLogEntry entry)
        {
            computerNameTextBox.Text = entry.computerName;
            serviceNameTextBox.Text = entry.serviceName;
            eventDateTextBox.Text = entry.recordDate;
            eventTimeTextBox.Text = entry.recordTime;
            eventTimeStampTextBox.Text = entry.eventTimestamp;
            packetTypeTextBox.Text = entry.packetType;
            userNameTextBox.Text = entry.userName;
            fqdnTextBox.Text = entry.fullyQualifiedDistinguishedName;
            calledStationIDTextBox.Text = entry.calledStationID;
            callingStationIDTextBox.Text = entry.callingStationID;
            callbackNumberTextBox.Text = entry.callbackNumber;
            framedIPAddressTextBox.Text = entry.framedIPAddress;
            nasIdentifierTextBox.Text = entry.nasIdentifier;
            nasIPAddressTextBox.Text = entry.nasIPAddress;
            nasPortTextBox.Text = entry.nasPort;
            clientVendorTextBox.Text = entry.clientVendor;
            clientIPAddressTextBox.Text = entry.clientIPAddress;
            clientFriendlyNameTextBox.Text = entry.clientFriendlyName;
            portLimitTextBox.Text = entry.portLimit;
            nasPortTypeTextBox.Text = entry.nasPortType;
            connectInfoTextBox.Text = entry.connectInfo;
            framedProtocolTextBox.Text = entry.framedProtocol;
            serviceTypeTextBox.Text = entry.serviceType;
            authenticationTypeTextBox.Text = entry.authenticationType;
            policyNameTextBox.Text = entry.policyName;
            reasonCodeTextBox.Text = entry.reasonCode;
            classAttributeTextBox.Text = entry.classAttribute;
            sessionTimeoutTextBox.Text = entry.sessionTimeout;
            idleTimeoutTextBox.Text = entry.idleTimeout;
            terminationActionTextBox.Text = entry.terminationAction;
            eapFriendlyNameTextBox.Text = entry.eapFriendlyName;
            accountStatusTypeTextBox.Text = entry.acctStatusType;
            accountDelayTimeTextBox.Text = entry.acctDelayTime;
            accountInputOctetsTextBox.Text = entry.acctInputOctets;
            accountOutputOctetsTextBox.Text = entry.acctOutputOctets;
            accountSessionIDTextBox.Text = entry.acctSessionId;
            accountAuthenticTextBox.Text = entry.acctAuthentic;
            accountSessionTimeTextBox.Text = entry.acctSessionTime;
            accountInputPacketsTextBox.Text = entry.acctInputPackets;
            accountOutputPacketsTextBox.Text = entry.acctOutputPackets;
            accountTerminateCauseTextBox.Text = entry.acctTerminateCause;
            accountMultiSSNTextBox.Text = entry.acctMultiSsnID;
            accountLinkCountTextBox.Text = entry.acctLinkCount;
            accountInterimIntervalTextBox.Text = entry.acctInterimInterval;
            tunnelTypeTextBox.Text = entry.tunnelType;
            tunnelMediumTypeTextBox.Text = entry.tunnelType;
            tunnelClientEndpointTextBox.Text = entry.tunnelClientEndpt;
            tunnelServerEndpointTextBox.Text = entry.tunnelServerEndpt;
            accountTunnelConnTextBox.Text = entry.acctTunnelConn;
            tunnelPrivateGroupTextBox.Text = entry.tunnelPvtGroupID;
            tunnelAssignmentIDTextBox.Text = entry.tunnelAssignmentID;
            tunnelPreferenceTextBox.Text = entry.tunnelPreference;
            msAccountAuthTypeTextBox.Text = entry.msAcctAuthType;
            msAccountEAPTypeTextBox.Text = entry.msAcctEAPType;
            msRASVersionTextBox.Text = entry.msRASVersion;
            msRASVendorTextBox.Text = entry.msRASVendor;
            msCHAPErrorTextBox.Text = entry.msCHAPError;
            msCHAPDomainTextBox.Text = entry.msCHAPDomain;
            msMPPEEncryptionTypesTextBox.Text = entry.msMPPEEncryptionTypes;
            msMPPEEncryptionPolicyTextBox.Text = entry.msMPPEEncryptionPolicy;
            proxyPolicyNameTextBox.Text = entry.proxyPolicyName;
            providerTypeTextBox.Text = entry.providerType;
            providerNameTextBox.Text = entry.providerName;
            remoteServerAddressTextBox.Text = entry.remoteServerAddress;
            msRASClientNameTextBox.Text = entry.mSRASClientName;
            msRASClientVersionTextBox.Text = entry.mSRASClientVersion;
        }
    }
}
